import * as hlApi from "heroeslounge-api"
import * as _ from "lodash/fp"
import {rate_1vs1, Rating, winProbability} from "ts-trueskill"
import {DateTime} from "luxon"
import * as yargs from "yargs"
import * as flatCache from "flat-cache"
import * as numeral from "numeral"


const args = yargs
    .option("season", {
        alias: "s",
        demandOption: true,
        desc: 'a season, examples: "euseason-10", "na-aram-league", "na-division-s-season-1", "season-7"',
        type: "string",
    })
    .option("division", {
        alias: "d",
        demandOption: true,
        desc: 'a division, examples: "division-4", "group-b"',
        type: "string",
    })
    .option("cache", {
        alias: "c",
        desc: 'load data from cache?"',
        type: "boolean",
    })
    .argv;

let cache = flatCache.load("cache");


const _wrapFlowAsync = (fn) => (arg) => new Promise(resolve => resolve(arg))
    .then(fn);

const flowAsync = (...fns) => {
    const wrappedFns = fns.map(fn => _wrapFlowAsync(fn));
    return _.flow(wrappedFns)
};

const score = (rating: Rating) => rating.mu - 3.0 * rating.sigma;

// returns true if a wins the simulation
const simulateGame = ([a, b]: [Rating, Rating]): boolean => Math.random() < winProbability([a], [b]);

// returns the scores of the teams after the simulated match
const simulateMatch = ([a, b]: [Rating, Rating], limit = 2): [number, number] => {
    let scores: [number, number] = [0, 0];
    const game = () => {
        if (simulateGame([a, b])) {
            scores[0]++;
        } else {
            scores[1]++;
        }
    };
    while (!_.any(n => n >= limit)(scores)) {
        game();
    }
    return scores;
};

const parseDate = (hlApiDate?: string | null): DateTime =>
    DateTime.fromFormat(_.defaultTo("", hlApiDate), 'yyyy-MM-dd hh:mm:ss', {zone: 'utc'});


(async () => {
    let allSeasons;
    if (args.cache && cache.getKey("seasons") != null) {
        allSeasons = cache.getKey("seasons");
    } else {
        allSeasons = await hlApi.getSeasons();
        cache.setKey("seasons", allSeasons);
        cache.save(true);
    }

    let season = _.flow(
        _.filter((season: any) => season.slug == args.season),
        _.head,
    )(allSeasons);
    if (season == null) {
        console.log(`Could not find season "${args.season}"`);
        return;
    }
    console.log(`Season: ${season.title}`);

    let division = _.flow(
        _.filter((division: any) => division.slug == args.division),
        _.head,
    )(await hlApi.getSeasondivisions(season.id));
    if (division == null) {
        console.log(`Could not find division "${args.division}"`);
        return;
    }
    console.log(`Division: ${division.title}`);


    let allMatches;
    if (args.cache && cache.getKey("matches") != null) {
        console.log("Loading matches from cache");
        allMatches = cache.getKey("matches");
    } else {
        console.log("Searching for matches");
        allMatches = await hlApi.getMatches();
        cache.setKey("matches", allMatches);
        cache.save(true);
    }

    let matches = _.filter((match: any) => match.div_id == division.id)(allMatches);
    if (matches.length == 0) {
        console.log("No matches found");
        return;
    }
    console.log(`Found ${matches.length} matches`);


    let allGames;
    if (args.cache && cache.getKey("games") != null) {
        console.log("Loading games from cache");
        allGames = cache.getKey("games");
    } else {
        console.log("Searching for games");
        allGames = await hlApi.getGames();
        cache.setKey("games", allGames);
        cache.save(true);
    }

    let matchIds = _.map((match: any) => match.id)(matches);
    let games = _.filter((game: any) => _.includes(game.match_id)(matchIds))(allGames);
    if (games.length == 0) {
        console.log("No games found");
        return;
    }
    console.log(`Found ${games.length} games`);
    // _.each((game: any) => console.log(game))(games);


    console.log("Querying Teams");
    let teamMap: Map<number, any> = await flowAsync(
        _.flatMap((game: any) => [game.team_one_id, game.team_two_id]),
        _.filter((game: any) => !_.isNil(game)),
        _.uniq,
        _.map(async (teamId: number) => [teamId, await hlApi.getTeam(teamId)]),
        async (teams: any) => new Map(await Promise.all(teams)),
    )(games);

    console.log(`Found ${teamMap.size} teams`);
    // console.log(teamMap);

    console.log("Calculating ratings");
    let teamToRating: Map<number, Rating> = _.flow(
        _.map((teamId: number) => [teamId, new Rating()]),
        (ratings: [number, Rating][]) => new Map(ratings),
    )([...teamMap.keys()]);


    console.log("\nRatings:\n");

    for (let game of _.flow(
        _.filter((game: any) => !_.isNil(game.team_one_id) && !_.isNil(game.team_two_id)),
        _.sortBy((game: any) => parseDate(game.created_at)),
    )(games)) {
        let winnerId: number = game.winner_id;
        let loserId: number = game.team_one_id != winnerId ? game.team_one_id : game.team_two_id;
        _.flow(
            _.map((teamId: number) => teamToRating.get(teamId)),
            ([winnerRating, loserRating]) => {
                if (winnerRating == null || loserRating == null) {
                    console.log(winnerId, loserId);
                    console.log(teamMap.get(winnerId), teamMap.get(loserId));
                }
                return rate_1vs1(winnerRating, loserRating);
            },
            _.zip([winnerId, loserId]),
            _.each(([id, rating]) => teamToRating.set(id, rating)),
        )([winnerId, loserId]);
    }

    _.flow(
        _.map(([id, rating]) => ({id, rating})),
        _.sortBy((team: { id: number, rating: Rating }) => score(team.rating)),
        _.reverse,
        _.map((team: { id: number, rating: Rating }) => _.merge(team)(teamMap.get(team.id))),
    )([...teamToRating.entries()]).forEach((team: any, index: number) => {
        let s = score(team.rating);
        const fmt = n => numeral(n).format("0.00");
        console.log(`[${index + 1}] ${team.title}: Score: ${fmt(s)} (µ: ${fmt(team.rating.mu)}, σ: ${fmt(team.rating.sigma)})`);
    });


    console.log("\nUpcoming match simulations:\n");

    const predictionMessages = await flowAsync(
        _.filter((match: any) => match.is_played == 0),
        _.shuffle,
        _.partition((match: any) => parseDate(match.wbp).isValid),
        ([withTime, others]: [any[], any[]]) =>
            [..._.sortBy((match: any) => parseDate(match.wbp), withTime), ...others],
        _.map(async (match: any) => {
            const teams = await hlApi.getMatchTeams(match.id);
            // @ts-ignore
            const ratings: [Rating, Rating] = _.map((team: any) => teamToRating.get(team.id))(teams);
            const scoreCounts: Map<string, number> = new Map([
                ["2-0", 0],
                ["2-1", 0],
                ["1-2", 0],
                ["0-2", 0],
            ]);
            const totalScores: number = 10000;
            for (let i = 0; i < totalScores; i++) {
                let score = simulateMatch(ratings);
                const key = `${score[0]}-${score[1]}`;
                scoreCounts.set(key, scoreCounts.get(key) + 1);
            }
            const scoreString = _.flow(
                _.sortBy(([, count]: [[number, number], number]) => count),
                _.reverse,
                _.map(([score, count]: [string, number]) => {
                    return `${score}: ${numeral(count / totalScores).format("0.0%")}`
                }),
                _.join("\n"),
            )([...scoreCounts.entries()]);
            return `[${match.round}] ${teams[0].title} - ${teams[1].title}:\n${scoreString}`;
        }),
        async (promises: any) => await Promise.all(promises),
    )(matches);

    // const predictionMessages = await flowAsync(
    //     _.map(async (team: any) => {
    //         const teams = [teamMap.get(1259), team];
    //         // @ts-ignore
    //         const ratings: [Rating, Rating] = _.map((team: any) => teamToRating.get(team.id))(teams);
    //         const scoreCounts: Map<string, number> = new Map([
    //             ["2-0", 0],
    //             ["2-1", 0],
    //             ["1-2", 0],
    //             ["0-2", 0],
    //         ]);
    //         const totalScores: number = 10000;
    //         for (let i = 0; i < totalScores; i++) {
    //             let score = simulateMatch(ratings);
    //             const key = `${score[0]}-${score[1]}`;
    //             scoreCounts.set(key, scoreCounts.get(key) + 1);
    //         }
    //         const scoreString = _.flow(
    //             _.sortBy(([_, count]: [[number, number], number]) => count),
    //             _.reverse,
    //             _.map(([score, count]: [string, number]) => {
    //                 return `${score}: ${numeral(count / totalScores).format("0.00%")}`
    //             }),
    //             _.join("\n"),
    //         )([...scoreCounts.entries()]);
    //         return `${teams[0].title} - ${teams[1].title}:\n${scoreString}`;
    //     }),
    //     async (promises: any) => await Promise.all(promises),
    // )([...teamMap.values()]);

    for (let predictionMessage of predictionMessages) {
        console.log(predictionMessage);
    }
})();
